import Head from 'next/head'
import {AnimatePresence} from 'framer-motion';
import {ConfigProvider} from 'antd';
import {RouterAccessControl} from '../components/Auth/Auth';
import BooksProvider from '../contexts/BooksProvider';
import NotificationsProvider from '../contexts/NotificationsProvider';
import 'antd/dist/reset.css';
import '../styles/globals.css'

export default function App({ Component, pageProps }) {

  return (
    <ConfigProvider theme={{
      token: {
        borderRadius: 0,
        colorPrimary: '#35d796'
      },
      components: {
        Button: {
          defaultBorderColor: '#35d796',
          defaultColor: '#35d796',
          colorLink: '#35d796',
          colorLinkActive: '#35d796',
          colorLinkHover: '#35d796'
        }
      }
    }}>
      <Head>
        <title>Booklist</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <RouterAccessControl>
        <AnimatePresence exitBeforeEnter>
          <NotificationsProvider>
            <BooksProvider>
              <Component {...pageProps} />
            </BooksProvider>
          </NotificationsProvider>
        </AnimatePresence>
      </RouterAccessControl>
    </ConfigProvider>
  )
}
