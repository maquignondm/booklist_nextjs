import {useEffect, useState} from 'react';
import { useRouter } from 'next/router';
import {Col, Row} from 'antd';
import BooksList from '../components/Books/BooksList';
import CustomLayout from '../components/Layout/CustomLayout';
import {getBooks} from '../requests/BooksRequest';

export default function Searched() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  useEffect(() => {
    if(router.query.query) {
      refreshData(router.query.query);
    }
  }, [router.query])

  /**
   *
   * @param query
   */
  const refreshData = (query) => {
    setLoading(true);
    getBooks(query).then(res => {
      res && res.items ? setData(res.items) : null;
      setLoading(false);
    })
  }

  return <CustomLayout spinning={loading}>
    <Row gutter={[16, 16]} justify='center'>
      <Col span={20}>
        <BooksList data={data} />
      </Col>
    </Row>
  </CustomLayout>
}