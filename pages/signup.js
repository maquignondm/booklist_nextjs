import { useRouter } from 'next/router';
import AuthForm from '../components/Auth/AuthForm';
import { signup } from '../requests/AuthRequests';
import {useNotifications} from '../contexts/NotificationsProvider';

export default function Signup() {
  const router = useRouter();
  const notifications = useNotifications();

  const onSubmit = async (mail, password) => {
    signup(mail, password)
      .then((res) => {
        if(res.uid) {
          router.push('signin');
        }
      })
      .catch(() => notifications.openNotification('Erreur', 'Un problème a été rencontré.', 'error'));
  }

  return <AuthForm type='signup' onSubmit={onSubmit} />
}