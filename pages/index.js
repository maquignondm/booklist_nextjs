import {useEffect, useState} from 'react';
import {Row, Col, Pagination, Select, Input, Button} from 'antd';
import BooksList from '../components/Books/BooksList';
import CustomLayout from '../components/Layout/CustomLayout';
import {useBooks} from '../contexts/BooksProvider';
import {SearchOutlined, RetweetOutlined} from '@ant-design/icons';

export default function Index() {
  const [fileredBooks, setFilteredBooks] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentPageSize, setCurrentPageSize] = useState(24);
  const [searched, setSearched] = useState('');
  const [filteredTag, setFilteredTag] = useState(null);
  const [filteredStatus, setFilteredStatus] = useState(null);
  const [filteredAuthor, setFilteredAuthor] = useState(null);
  const [sortingType, setSortingType] = useState('datesAsc');
  const userBooks = useBooks();

  useEffect(() => {
    if(!userBooks.books || userBooks.books.length <= 0) return;
    let books = userBooks.books;
    if(searched.length > 1) {
      let searchedLower = searched.toLowerCase();
      books = books.filter(b => b.title.toLowerCase().includes(searchedLower) || b.subtitle?.toLowerCase().includes(searchedLower));
    }
    if(filteredTag) {
      books = books.filter(b => (b.wish_list && filteredTag === 'wish_list' || b.read && filteredTag === 'read' || b.library && filteredTag === 'library'));
    }
    if(filteredStatus) {
      books = books.filter(b => (filteredStatus === 'in_progress' && b.pages > 0 && b.pages < b.pageCount) ||
        (filteredStatus === 'not_started' && (b.pages <= 0 || !b.pages)) ||
        (filteredStatus === 'done' && b.pages === b.pageCount));
    } 
    if(filteredAuthor) {
      books = books.filter(b => b.authors.includes(filteredAuthor));
    }

    setFilteredBooks(books);
  }, [userBooks.books, searched, filteredTag, filteredStatus, filteredAuthor])

  /**
   * Fonction permettant de trier les livres par date
   * 
   * @param {*} current 
   * @returns 
   */
  const sortBooks = (current) => {
    if(sortingType === 'datesAsc') {
      return current.sort((a, b) => new Date(a.publishedDate) - new Date(b.publishedDate));
    } else if (sortingType === 'datesDesc') {
      return current.sort((a, b) => new Date(b.publishedDate) - new Date(a.publishedDate));
    } else if (sortingType === 'titlesAsc') {
      return current.sort((a, b) => {
        if (a.title < b.title){
          return -1;
        }
        if (a.title > b.title){
          return 1;
        }
        return 0;
      });
    } else if (sortingType === 'titlesDesc') {
      return current.sort((a, b) => {
        if (a.title > b.title){
          return -1;
        }
        if (a.title < b.titl){
          return 1;
        }
        return 0;
      });
    }
  }

  /**
   * Génére la liste des options pour le filtre auteurs
   * 
   * @returns 
   */
  const authorsOptions = () => {
    let authors = {};
    userBooks.books.forEach(b => {
      b.authors ? b.authors.forEach(a => authors[a] = a) : null;
    });

    return Object.values(authors).sort().map(a => ({value: a, label: a}));
  }

  /**
   * Fonction permettant de gérer la pagination
   * 
   * @param {*} page
   * @param {*} pageSize
   */
  const onChangePagination = (page, pageSize) => {
    setCurrentPage(page);
    setCurrentPageSize(pageSize);
  }

  return <CustomLayout spinning={false}>
    <Row gutter={[16, 16]} justify='center' hidden={userBooks.books.length <= 0}>
      <Col span={22} style={{marginTop: '25px', paddingRight: '16px', paddingLeft: '16px'}}>
        <Row gutter={[16, 16]} justify='center'>
          <Col xs={24} lg={4}>
            <Input
              placeholder='Rechercher...'
              prefix={<SearchOutlined  />}
              style={{ width: '100%' }}
              onChange={(e) => setSearched(e.target.value)}
            />
          </Col>
          <Col xs={24} lg={4}>
            <Button 
              icon={<RetweetOutlined />} 
              style={{width: '100%'}}
              onClick={() => setSortingType(o => o === 'datesAsc' ? 'datesDesc' : 'datesAsc')}
            >
              Trier par dates
            </Button>
          </Col>
          <Col xs={24} lg={4}>
            <Button 
              icon={<RetweetOutlined />} 
              style={{width: '100%'}}
              onClick={() => setSortingType(o => o === 'titlesAsc' ? 'titlesDesc' : 'titlesAsc')}
            >
              Trier par titres
            </Button>
          </Col>
          <Col xs={24} lg={4}>
            <Select
              placeholder='Filtrer par tag'
              allowClear={true}
              onChange={setFilteredTag}
              style={{ width: '100%' }}
              options={[
                { value: 'library', label: 'Bibliothèque' },
                { value: 'wish_list', label: 'Liste de souhait' },
                { value: 'read', label: 'Déjà lu' }
              ]}
            />
          </Col>
          <Col xs={24} lg={4}>
            <Select
              placeholder='Filtrer par status'
              allowClear={true}
              onChange={setFilteredStatus}
              style={{ width: '100%' }}
              options={[
                { value: 'not_started', label: 'Pas commencé' },
                { value: 'in_progress', label: 'En cours de lecture' },
                { value: 'done', label: 'Terminé' },
              ]}
            />
          </Col>
          <Col xs={24} lg={4}>
            <Select
              showSearch
              placeholder='Filtrer par auteur'
              allowClear={true}
              onChange={setFilteredAuthor}
              style={{ width: '100%' }}
              options={authorsOptions()}
            />
          </Col>
        </Row>
      </Col>
      <Col span={22}>
        <BooksList data={sortBooks(fileredBooks).slice((currentPage - 1) * currentPageSize, currentPageSize * currentPage)} />
      </Col>
      <Col span={24} style={{marginBottom: '25px', display: 'flex', justifyContent: 'center'}}>
        <Pagination
          defaultCurrent={1}
          total={fileredBooks.length}
          onChange={onChangePagination}
          pageSize={currentPageSize}
          current={currentPage}
        />
      </Col>
    </Row>
  </CustomLayout>
}
