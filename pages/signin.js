import { useRouter } from 'next/router';
import AuthForm from '../components/Auth/AuthForm';
import { signin } from '../requests/AuthRequests';
import {useNotifications} from '../contexts/NotificationsProvider';

export default function Signin() {
  const router = useRouter();
  const notifications = useNotifications();

  const onSubmit = async (mail, password) => {
    signin(mail, password)
      .then(res => {
        if(res.uid) {
          router.push('/');
        }
      })
      .catch(() => notifications.openNotification('Erreur', 'Un problème a été rencontré. Vérifiez vos informations de connexion.', 'error'));
  }

  return <AuthForm type='signin' onSubmit={onSubmit} />
}