import axios from 'axios';
import {addDoc, collection, deleteDoc, doc, getDocs, query, serverTimestamp, updateDoc, where} from 'firebase/firestore'
import {db} from '../services/firebase.config.js';

const bookRef = collection(db, 'books');

/**
 * Fonction permettant de récupérer des livres depuis l'API Google Books
 *
 * @param query
 * @param max
 * @param page
 * @returns {Promise<any>}
 */
export const getBooks = async (query, max = 20, page = 1) => {
  try {
    if(query.length <= 3) return;
    let response = await axios.get(`https://www.googleapis.com/books/v1/volumes?q=${query}&maxResults=${max}&startIndex=${page}`);
    response.data.items = response.data.items.map(res => {
      return formatBookResponse(res);
    });

    return response.data;
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * Fonction permettant de récupérer un livre depuis l'API Google Books
 *
 * @param id
 * @returns {Promise<any>}
 */
export const getBookById = async (id) => {
  try {
    if(!id) return;
    let response = await axios.get(`https://www.googleapis.com/books/v1/volumes/${id}`);
    let res = response.data;

    return formatBookResponse(res);
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * Fonction permettant de formatter les résultats de l'API Google Books
 *
 * @param {*} res
 * @returns
 */
const formatBookResponse = (res) => {
  let current = {};
  if(res.volumeInfo) {
    current.id = res.id;
    current.title = res.volumeInfo.title ? res.volumeInfo.title : null;
    current.subtitle = res.volumeInfo.subtitle ? res.volumeInfo.subtitle : null;
    current.publisher = res.volumeInfo.publisher ? res.volumeInfo.publisher : null;
    current.description = res.volumeInfo.description ? res.volumeInfo.description : null;
    current.publishedDate = res.volumeInfo.publishedDate ? res.volumeInfo.publishedDate : null;
    current.authors = res.volumeInfo.authors ? res.volumeInfo.authors : [];
    current.pageCount = res.volumeInfo.pageCount ? res.volumeInfo.pageCount : null;
    current.printedPageCount = res.volumeInfo.printedPageCount ? res.volumeInfo.printedPageCount : null;
    current.language = res.volumeInfo.language ? res.volumeInfo.language : null;
    if(res.volumeInfo.imageLinks) {
      current.smallThumbnail = res.volumeInfo.imageLinks.smallThumbnail;
      current.thumbnail = res.volumeInfo.imageLinks.thumbnail;
    }
    if(res.volumeInfo.industryIdentifiers) {
      current.industryIdentifiers = res.volumeInfo.industryIdentifiers;
    }
  }

  return current;
}

/**
 * Fonction permettant d'insérer un livre
 *
 * @param {*} data
 */
export const insertBook = async (data) => {
  let finalData = {...data, timestamp: serverTimestamp()}
  try {
    return await addDoc(bookRef, finalData);
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * Fonction permettant de récupérer les livres d'un utilisateur
 *
 * @param uid
 */
export const getBooksByUserId = async (uid) => {
  const q = query(bookRef, where('uid', '==', uid));
  try {
    const querySnapshot = await getDocs(q);
    let tmpBooks = [];
    querySnapshot.forEach((doc) => {
      tmpBooks.push({...doc.data(), doc_id: doc.id})
    });

    return tmpBooks;
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * Fonction permettant de récupérer tous les livres
 */
export const getAllBooks = async () => {
  try {
    const querySnapshot = await getDocs(bookRef);
    let tmpBooks = [];
    querySnapshot.forEach((doc) => {
      tmpBooks.push({...doc.data(), doc_id: doc.id})
    });

    return tmpBooks;
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * Fonction permettant de supprimer un livre par son document ID
 *
 * @param {*} id
 */
export const deleteBookByDocId = async (id) => {
  try {
    const documentRef = doc(db, 'books', id);
    await deleteDoc(documentRef);
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * Fonction permettant de mettre à jour un livre par son document ID
 *
 * @param {*} id
 * @param {*} book
 */
export const updateBookByDocId = async (id, book) => {
  try {
    const documentRef = doc(db, 'books', id);
    await updateDoc(documentRef, book);
  } catch (e) {
    console.error(e);
    throw e;
  }
}