import {  createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from 'firebase/auth';
import { auth } from '../services/firebase.config';

/**
 *
 * @param mail
 * @param password
 * @returns {Promise<User>}
 */
export const signup = async (mail, password) => {
  try {
    let res = await createUserWithEmailAndPassword(auth, mail, password);
    return res.user;
  } catch (e) {
    throw e;
  }
}

/**
 *
 * @param mail
 * @param password
 * @returns {Promise<User>}
 */
export const signin = async (mail, password) => {
  try {
    let res = await signInWithEmailAndPassword(auth, mail, password);
    if(res.user) {
      window.localStorage.setItem('booklist_user_connected', JSON.stringify({is_connected: true, uid: res.user.uid}));
      return res.user;
    }
  } catch (e) {
    throw e;
  }
}

export const signout = async () => {
  try {
    window.localStorage.removeItem('booklist_user_connected');
    await signOut(auth);
  } catch (e) {
    throw e;
  }
}