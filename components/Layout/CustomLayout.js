import {useEffect, useState} from 'react';
import { useRouter } from 'next/router';
import {Layout, Row, Col, Button, AutoComplete, Spin, Drawer} from 'antd';
import {SearchOutlined, MenuOutlined, SettingOutlined} from '@ant-design/icons';
import {motion} from 'framer-motion';
import {getBooksByUserId, getBooks} from '../../requests/BooksRequest';
import { useBooks } from '../../contexts/BooksProvider';
import CustomMenu from './CustomMenu';

const { Header, Content } = Layout;

export default function CustomLayout({children, spinning}) {
  const [search, setSearch] = useState('');
  const [options, setOptions] = useState([]);
  const [keyPress, setKeyPress] = useState(null);
  const [openMenu, setOpenMenu] = useState(false);
  const router = useRouter();
  const userBooks = useBooks();

  const variants = {
    visible: { opacity: 1 },
    hidden: { opacity: 0 }
  };

  useEffect(() => {
    const handleKeyUp = (event) => {
      switch (event.key) {
        case "Enter":
          event.preventDefault();
          setKeyPress(event.key);
          break;
      }
    }
    window.addEventListener("keyup", handleKeyUp);
    let userStored = window.localStorage.getItem('booklist_user_connected');
    if(!userStored) return;
    userStored = JSON.parse(userStored);
    getBooksByUserId(userStored.uid).then(res => userBooks.setBooks(res));
    return () => window.removeEventListener("keyup", handleKeyUp);
  }, [])

  useEffect(() => {
    if(keyPress === 'Enter') {
      refreshData(search);
      setKeyPress(null);
    }
  }, [keyPress])

  /**
   *
   */
  const refreshData = () => {
    setSearch('');
    router.push(`/searched?query=${search}`);
  }

  /**
   *
   * @param text
   */
  const onSearch = (text) => {
    getBooks(text, 5).then(res => {
      if (!res || !res.items) {
        return;
      }
      setOptions(res.items.map(item => ({
        value: item.id,
        label: `${item?.title ? item?.title : ''} ${item.subtitle ? ' - ' + item.subtitle : ''}`
      })))
    });
  }

  /**
   *
   * @param val
   */
  const onSelect = (val) => {
    let option = options.filter(o => o.value === val)[0];
    setSearch(option.label);
    setOptions([]);
    refreshData(option.label);
  }

  return <motion.div initial="hidden" animate="visible" exit="hidden" variants={variants} transition={{ duration: 0.4 }}>
    <Layout>
      <Header style={{
        background: '#FFFFFF',
        borderBottom: '1px solid #DDDDDD',
        padding: '0px',
        position: 'fixed',
        width: '100%',
        zIndex: 1000
      }}>
        <Row justify='space-between'>
          <Col>
            <Button
              type='text'
              style={{marginLeft: '15px'}}
              icon={<MenuOutlined />}
              onClick={() => setOpenMenu(!openMenu)}
            />
          </Col>
          <Col xs={14} span={20} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '64px' }}>
            <AutoComplete
              options={options}
              style={{ width: '80%' }}
              value={search}
              onSelect={onSelect}
              onSearch={(text) => onSearch(text)}
              onChange={(text) => setSearch(text)}
              placeholder='Rechercher des livres...'
            />
            <Button type='primary' onClick={() => refreshData(search)} icon={<SearchOutlined/>}/>
          </Col>
          <Col>
            <Button type='text' style={{marginRight: '15px', color: '#FFFFFF'}} icon={<SettingOutlined />} />
          </Col>
        </Row>
      </Header>
      <Layout>
        <Content style={{ marginTop: '64px', minHeight: 'calc(100vh - 64px)' }}>
          <Spin spinning={spinning}>
            {children}
          </Spin>
        </Content>
      </Layout>
      <Drawer
        open={openMenu}
        onClose={() => setOpenMenu(false)}
        placement='left' width={250}
        bodyStyle={{padding: '0'}}
      >
        <CustomMenu />
      </Drawer>
    </Layout>
  </motion.div>
}