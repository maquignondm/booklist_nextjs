import {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import Link from 'next/link';
import {Menu} from 'antd';
import { ReadOutlined, PoweroffOutlined  } from '@ant-design/icons';
import {signout} from '../../requests/AuthRequests';
import { useBooks } from '../../contexts/BooksProvider';

export default function CustomMenu () {
  const [keys, setKeys] = useState([]);
  const router = useRouter();
  const userBooks = useBooks();

  useEffect(() => {
    let currentKey = router.pathname === '/' ? 'my-books' : router.pathname.substring(1);
    setKeys([currentKey]);
  }, [router])

  /**
   *
   * @param {*} label
   * @param {*} key
   * @param {*} icon
   * @param {*} children
   * @param {*} type
   * @returns
   */
  const getItem = (label, key, icon, children, type) => {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }

  /**
   *
   */
  const onLogout = () => {
    signout();
    router.push('/signin');
    userBooks.setBooks([]);
  }

  const items = [
    getItem(
      <Link href='/'>Mes livres</Link>,
      'my-books',
      <ReadOutlined />
    ),
    getItem(
      <div onClick={onLogout}>Déconnexion</div>,
      'logout',
      <PoweroffOutlined  />
    ),
  ];

  return (
    <Menu
      defaultSelectedKeys={['1']}
      defaultOpenKeys={['sub1']}
      selectedKeys={keys}
      mode='inline'
      items={items}
    />
  );
};