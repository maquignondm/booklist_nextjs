/**
 *
 * @param {*} email
 * @returns
 */
export const validateEmail = (email) => {
  return String(email)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};

/**
 *
 * @param {*} charDate
 * @returns
 */
export const formatDateFr = (charDate) => {
  const date = new Date(charDate);
  if (isNaN(date)) {
    return 'Date invalide';
  }
  const jour = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  const mois = (date.getMonth() + 1) < 10 ? '0' + (date.getDate() + 1) : (date.getDate() + 1);
  const annee = date.getFullYear();

  return `${jour}/${mois}/${annee}`;
}