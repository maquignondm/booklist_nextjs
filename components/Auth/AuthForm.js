import {useState, useEffect} from 'react';
import {useRouter} from 'next/router';
import {Button, Card, Checkbox, Col, Divider, Input, Row} from 'antd';
import {MailOutlined, LockOutlined, EyeInvisibleOutlined, EyeTwoTone, UserOutlined } from '@ant-design/icons';
import {validateEmail} from '../Utils';

export default function AuthForm({type, onSubmit}) {
  const [mail, setMail] = useState(null);
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [confirm, setConfirm] = useState(null);
  const [saveAuth, setSaveAuth] = useState(false);
  const [status, setStatus] = useState({});
  const [loading, setLoading] = useState(false);
  const [keyPress, setKeyPress] = useState(null);
  const router = useRouter();

  useEffect(() => {  
    const handleKeyUp = (event) => {
      switch (event.key) {
        case "Enter":
          event.preventDefault();
          setKeyPress(event.key);
          break;
      }
    }
  
    window.addEventListener("keyup", handleKeyUp);
    return () => window.removeEventListener("keyup", handleKeyUp);
  }, []);

  useEffect(() => {
    if(keyPress === 'Enter') {
      onClick();
      setKeyPress(null);
    };
  }, [keyPress])

  const onClick = () => {
    let tmp = {};
    if(!mail || mail === '' || !validateEmail(mail)) tmp.mail = 'error';
    if(type !== 'signin' && (!username || username === '')) tmp.username = 'error';
    if(!password || password === '' || (type === 'signup' && password !== confirm)) tmp.password = 'error';
    if(type !== 'signin' && (!confirm || confirm === '' || (type === 'signup' && password !== confirm))) tmp.confirm = 'error';
    setStatus(tmp);
    if(Object.values(tmp).length > 0) return;
    setLoading(true);
    onSubmit(mail, password).then(() => {
      setLoading(false);
    });
  }

  return <div className='auth-background' style={{
    backgroundColor: '#f5f5f5',
    height: '100%'
  }}>
    <Row justify='center' align='middle' style={{height: '100%'}}>
      <Col style={{maxWidth: '500px', marginTop: '-5rem'}}>
        <Card>
          <Row>
            <Col span={24}>
              <h1 className='logo' style={{textAlign: 'center', marginBottom: 0}}>
                Booklist
              </h1>
            </Col>
          </Row>
          <Divider />
          <Row gutter={[16, 12]}>
            <Col span={24} style={{marginBottom: '15px'}} hidden={type === 'signin'}>
              <Input
                size='large'
                prefix={<UserOutlined  />}
                placeholder='John Doe'
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                status={status.username}
              />
            </Col>
            <Col span={24} style={{marginBottom: '15px'}}>
              <Input
                size='large'
                prefix={<MailOutlined  />}
                placeholder='mail@gmail.com'
                value={mail}
                onChange={(e) => setMail(e.target.value)}
                status={status.mail}
              />
            </Col>
            <Col span={24} style={{marginBottom: '15px'}}>
              <Input.Password
                size='large'
                prefix={<LockOutlined  />}
                placeholder='**************'
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                status={status.password}
              />
            </Col>
            <Col span={24} style={{marginBottom: '15px'}} hidden={type === 'signin'}>
              <Input.Password
                size='large'
                prefix={<LockOutlined  />}
                placeholder='**************'
                value={confirm}
                onChange={(e) => setConfirm(e.target.value)}
                iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                status={status.confirm}
              />
            </Col>
            <Col span={24} hidden={type === 'signup'}>
              <Checkbox
                onChange={(e) => setSaveAuth(e.target.checked)}
                style={{marginBottom: '15px'}}
                checked={saveAuth}
              >
                Rester connecté
              </Checkbox>
            </Col>
            <Col span={24}>
              <Button
                size='large'
                style={{width: '100%'}}
                type='primary'
                onClick={onClick}
                loading={loading}
              >
                {type === 'signup' ? 'S\'inscrire' : 'Se connecter'}
              </Button>
            </Col>
          </Row>
          <Divider />
          <Row>
            <Col span={24}>
              <Button
                onClick={() => router.push('/signin')}
                style={{width: '100%'}}
                type='link'
                hidden={type === 'signin'}
              >
                Se connecter
              </Button>
              <Button
                onClick={() => router.push('/signup')}
                style={{width: '100%'}}
                type='link'
                hidden={type === 'signup'}
              >
                S'inscrire
              </Button>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  </div>
}