import {useEffect, useState} from 'react';
import { useRouter } from 'next/router'

/**
 * Fonction permettant de vérifier l'accès aux routes
 * @param router
 * @param children
 * @returns {null}
 * @constructor
 */
export function RouterAccessControl({ children }) {
  const [view, setView] = useState(null);
  let router = useRouter();
  if(typeof window !== "undefined") {
    let user = window.localStorage.getItem('booklist_user_connected');
    user = user ? JSON.parse(user) : {};
  }
  useEffect(() => {
    if ((router.pathname === '/signin' || router.pathname === '/signup') && user.uid) {
      router.push('/');
    } else if ((router.pathname !== '/signin' && router.pathname !== '/signup') && !user.uid) {
      router.push('/signin');
    } else {
      setView(children);
    }
  }, [router.pathname]);

  return view;
}