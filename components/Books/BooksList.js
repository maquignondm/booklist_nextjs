import {useState} from 'react';
import {Col, Drawer, Row, message} from 'antd';
import BooksDetails from './BooksDetails';
import BookCard from './BookCard';

export default function BooksList({data}) {
  const [selectedBookId, setSelectedBookId] = useState(null);
  const [messageApi, contextHolder] = message.useMessage();

  /**
   * 
   * @returns 
   */
  const prepareData = () => [...new Set(data.map(item => item))];

  return (
    <Row gutter={[16, 16]} style={{ margin: '15px 0' }}>
      {contextHolder}
      {
        prepareData()?.map(d => <Col xs={24} sm={12} md={6} lg={4} key={`${d.id}-${d.index}`} style={{display: 'flex', justifyContent: 'center' }}>
          <BookCard book={d} setSelectedBookId={setSelectedBookId} messageApi={messageApi} />
        </Col>)
      }
      <Drawer
        destroyOnClose={true}
        bodyStyle={{padding: '0'}}
        width={600}
        title='Informations complémentaires'
        placement='right'
        onClose={() => setSelectedBookId(null)}
        open={selectedBookId}
      >
        <BooksDetails bookId={selectedBookId} />
      </Drawer>
    </Row>
  )
}