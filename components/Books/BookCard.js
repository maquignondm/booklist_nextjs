import { Button, Card, Tooltip } from 'antd';
import { DatabaseOutlined, FileSearchOutlined, FileDoneOutlined } from '@ant-design/icons';
import { useBooks } from '../../contexts/BooksProvider';
import { getAuth } from "firebase/auth";
import { useEffect, useState } from 'react';

const { Meta } = Card;

export default function BookCard({ book, setSelectedBookId, messageApi }) {
  const [image, setImage] = useState(null);
  const userBooks = useBooks();
  const auth = getAuth();

  useEffect(() => {
    if (book) {
      checkIfImageExists(`https://books.google.com/books/publisher/content/images/frontcover/${book.id}?fife=w480-h690`);
    }
  }, [book])

  /**
   * 
   * @param {*} bookId 
   * @param {*} key 
   * @returns 
   */
  const isPrimary = (bookId, key) => {
    let current = userBooks.books.filter(b => b.id === bookId)[0];
    if (current) {
      return current[key] ? 'default' : 'text';
    }

    return 'text';
  }

  /**
   * TODO Refacto doublon avec BooksDetailsTags
   * 
   * @param {*} key
   * @param {*} data
   */
  const onClick = (key, data) => {
    let currentBook = userBooks.mergeCurrentBookWithApiData(data.id, auth.currentUser.uid, data);
    let book = { ...currentBook, [key]: !currentBook[key] };
    userBooks.onBookAction(book).then(res => messageApi.success(res));
  }

  /**
   * 
   * @param {*} url 
   */
  const checkIfImageExists = (url) => {
    var image = new Image();
    image.addEventListener('load', function () {
      setImage('url(' + url + ')');
    });
    image.src = url;
  }

  return <Card
    style={{ width: '100%' }}
    bodyStyle={{ padding: '15px 5px' }}
    hoverable
    cover={<BookCardCover image={image} book={book} setSelectedBookId={setSelectedBookId} />}
    actions={[
      <Tooltip title='Bibliothèque' key='library'>
        <Button onClick={() => onClick('library', book)} type={isPrimary(book.id, 'library')} icon={<DatabaseOutlined />} />
      </Tooltip>,
      <Tooltip title='Liste de souhait' key='wish_list'>
        <Button onClick={() => onClick('wish_list', book)} type={isPrimary(book.id, 'wish_list')} icon={<FileSearchOutlined />} />
      </Tooltip>,
      <Tooltip title='Déjà lu' key='read'>
        <Button onClick={() => onClick('read', book)} type={isPrimary(book.id, 'read')} icon={<FileDoneOutlined />} />
      </Tooltip>
    ]}
  >
    <Meta
      title={book.title ? book.title : 'inconnu'}
      description={book.authors && book.authors.length > 0 ? book.authors[0] : book.publisher ? book.publisher : 'Inconnu'}
    />
  </Card>
}

function BookCardCover({ book, image, setSelectedBookId }) {
  return book.smallThumbnail || book.thumbnail ?
    <div
      onClick={() => setSelectedBookId(book.id)}
      style={{
        backgroundImage: image,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        aspectRatio: '2/3'
      }}
    >
      <div className={'img-loader ' + (image ? 'hidden' : '')} >
        <span className="loader"></span>
      </div>
    </div> :
    <div
      onClick={() => setSelectedBookId(book.id)}
      style={{
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center',
        color: '#FFFFFF',
        backgroundColor: '#0b1433',
        fontWeight: 'bold',
        fontSize: '1rem',
        aspectRatio: '2/3'
      }}
    >
      <div>{book.title}</div>
      <div>{book.authors ? book.authors[0] : ''}</div>
      <div>{book.publisher}</div>
    </div>
}