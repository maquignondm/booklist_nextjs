import { Button, Col, Row } from 'antd';
import { useEffect, useState } from 'react';
import { useBooks } from '../../contexts/BooksProvider';

export default function ({data}) {
  const [tags, setTags] = useState({ read: false, library: false, wish_list: false })
  const userBooks = useBooks();

  useEffect(() => {
    if(data.id) {
      setTags({
        read: data.read ? !!data.read : false,
        library: data.library ? !!data.library : false,
        wish_list: data.wish_list ? !!data.wish_list : false
      })
    }
  }, [data])

  const tagsItems = [
    { key: 'library', title: 'Bibliothèque' },
    { key: 'wish_list', title: 'Liste de souhait' },
    { key: 'read', title: 'Déjà lu' }
  ]

  /**
   *
   * @param {*} key
   */
  const onClick = (key) => {
    let newTags = {...tags, [key]: !tags[key]}
    let book = {...data, ...newTags};
    userBooks.onBookAction(book).then(() => {
      setTags(newTags);
    });
  }

  return <Row gutter={[16, 12]}>
    {
      tagsItems.map(t => <Col key={t.key} span={8}>
        <Button
          type={tags[t.key] ? 'primary' : 'default'}
          style={{ width: '100%' }}
          size='small'
          onClick={() => onClick(t.key)}
        >
          {t.title}
        </Button>
      </Col>)
    }
  </Row>
}