import { Image } from 'antd';

export default function({data}) {
  return <div style={{
    backgroundImage: `url(${data.thumbnail ? data.thumbnail : data.smallThumbnail})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '40vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative'
  }}>
    <div
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.8)'}}
    ></div>
    <Image
      src={'https://books.google.com/books/publisher/content/images/frontcover/' + data.id + '?fife=w480-h690'}
      height={275}
      width={185}
    />
  </div>
}