import {useEffect, useState} from 'react';
import {Col, Divider, InputNumber, Progress, Row} from 'antd';
import {getBookById} from '../../requests/BooksRequest';
import BooksDetailsTags from './BooksDetailsTags';
import BooksDetailsImage from './BooksDetailsImage';
import {formatDateFr} from '../Utils';
import {useBooks} from '../../contexts/BooksProvider';
import {getAuth} from "firebase/auth";

export default function BooksDetails({bookId}) {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);
  const userBooks = useBooks();
  const auth = getAuth();

  useEffect(() => {
    if(bookId) {
      setLoading(true)
      getBookById(bookId).then(res => {
        let userBook = userBooks.mergeCurrentBookWithApiData(bookId, auth.currentUser.uid, res);
        setData(userBook);
        setLoading(false);
      });
    }
  }, [bookId])

  useEffect(() => {
    let currentBook = userBooks.books.filter(b => b.id === bookId)[0];
    if(currentBook) {
      setData(currentBook);
    }
  }, [userBooks.books])

  /**
   *
   * @param authors
   * @returns {*}
   */
  const showAuthors = (authors) => {
    if(authors) {
      return authors.map((a, i) => {
        return <span key={i}>
          {a} {i + 1 === authors.length ? '' : ' - '}
        </span>
      })
    }
  }

  /**
   *
   * @returns {number|number}
   */
  const calculPercent = () => {
    return data.pageCount ? Math.floor(100 * data.pages / data.pageCount) : 0
  }

  /**
   *
   * @param {*} num
   */
  const onChangeInputNumber = (num) => {
    let book = {...data, pages: num};
    setData(book);
    userBooks.onBookAction(book);
  }

  return !loading ? <>
    <BooksDetailsImage data={data} />
    <Divider style={{marginTop: '0'}} />
    <div style={{margin: '0 20px'}}>
      <Row gutter={[16, 12]}>
        <Col span={24}>
          <h3 className='title'>{data.title}</h3>
        </Col>
        <Col span={24}>
          <h4 className='subtitle'>{data.subtitle}</h4>
        </Col>
        <Col span={24}>
          <h4 className='subtitle'>{showAuthors(data.authors)}</h4>
        </Col>
      </Row>
      <Divider />
      <BooksDetailsTags data={data} />
      <Divider />
      <Row gutter={[16, 12]}>
        <Col span={12}>
          <h5 className='subtitle'>Editeur</h5>
          <p>{data.publisher}</p>
        </Col>
        <Col span={12}>
          <h5 className='subtitle'>Date d'édition</h5>
          <p>{formatDateFr(data.publishedDate)}</p>
        </Col>
        {
          data.industryIdentifiers ? data.industryIdentifiers.map(ii => <Col key={ii.identifier} span={12}>
            <h5 className='subtitle'>{ii.type}</h5>
            <p>{ii.identifier}</p>
          </Col>) : null
        }
        <Col span={12}>
          <h5 className='subtitle'>Nombre de page</h5>
          <p>{data.pageCount}</p>
        </Col>
        <Col span={18}>
          <Progress percent={calculPercent()} />
        </Col>
        <Col span={6}>
          <InputNumber
            controls={false}
            size='small'
            style={{width: '100%'}}
            min={0}
            defaultValue={0}
            value={data.pages ? data.pages : 0}
            onChange={onChangeInputNumber}
          />
        </Col>
      </Row>
      <Divider />
      <Row gutter={[16, 12]}>
        <Col span={24}>
          <h5 className='subtitle'>Description</h5>
          <p style={{textAlign: 'justify', marginBottom: '25px'}}>{data.description}</p>
        </Col>
      </Row>
    </div>
  </> : null
}