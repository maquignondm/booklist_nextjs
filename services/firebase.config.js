import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";

// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDUoai7jjDQ-WpP8eCLZF0XP6w5LS5Lmjs",
  authDomain: "booklist-3ecef.firebaseapp.com",
  projectId: "booklist-3ecef",
  storageBucket: "booklist-3ecef.appspot.com",
  messagingSenderId: "916409394018",
  appId: "1:916409394018:web:40848d01f1d44dae5c823b"
};

// Initialize Firebases
const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
export const auth = getAuth(app);
export default app;