import {createContext, useContext} from 'react';
import {notification} from 'antd';
import {ExclamationCircleOutlined} from '@ant-design/icons';

const NotificationsContext = createContext({});

function NotificationsProvider({children}) {

  const [api, contextHolder] = notification.useNotification();

  const openNotification = (message, description, type = 'info') => {

    const colors = {
      info: '#1677ff',
      error: '#ff4d4f',
      warning: '#ffcf24',
      success: '#35d796'
    };

    api.info({
      message,
      description,
      placement: 'topRight',
      icon: <ExclamationCircleOutlined style={{color: colors[type]}} />
    });
  };

  return (
    <NotificationsContext.Provider value={{ openNotification }}>
      {contextHolder}
      { children }
    </NotificationsContext.Provider>
  )
}

export default NotificationsProvider;

export const useNotifications = () => useContext(NotificationsContext);