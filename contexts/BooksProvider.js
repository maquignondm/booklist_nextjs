import {createContext, useContext, useState} from 'react';
import { deleteBookByDocId, insertBook, updateBookByDocId } from '../requests/BooksRequest';

const BooksContext = createContext({});

function BooksProvider({ children }) {
  const [books, setBooks] = useState([]);

  /**
   * @param book
   */
  const addBook = async book => {
    await insertBook(book).then(res => {
      book = {...book, doc_id: res.id}
      setBooks([...books, book]);
    })
  }

  /**
   * @param bookId
   */
  const deleteBook = async bookId => {
    let docId = books.filter(b => b.id === bookId)[0].doc_id;
    await deleteBookByDocId(docId).then(() => {
      let filtered = books.filter(b => b.id !== bookId);
      setBooks(filtered);
    })
  }

  /**
   * @param book
   */
  const updateBook = async (book) => {
    let currentBook = books.filter(b => b.id === book.id)[0];
    await updateBookByDocId(currentBook.doc_id, book).then(() => {
      let copy = [...books];
      let i = copy.findIndex(c => c.id === book.id);
      copy[i] = {...book, doc_id: currentBook.doc_id};
      setBooks(copy);
    })
  }

  /**
   *
   * @param {*} book
   */
  const onBookAction = async (book) => {
    let bookExist = books.filter(b => b.id === book.id).length > 0;
    if(!bookExist) {
      if(book.read && book.pageCount > 0) {
        book.pages = book.pageCount;
      } else if(book.pages > 0 && book.pages === book.pageCount) {
        book.read = true;
      }
      await addBook(book);
      return 'Le livre a bien été ajouté !';
    } else if (bookExist && (book.read || book.library || book.wish_list || book.pages > 0)) {
      if(book.read && book.pageCount > 0) {
        book.pages = book.pageCount;
      }
      await updateBook(book);
      return 'Le livre a bien été modifié !';
    } else if (bookExist) {
      await deleteBook(book.id);
      return 'Le livre a bien été supprimé !';
    } else {
      console.log('get books');
    }
  }

  /**
   *
   * @param bookId
   * @param uid
   * @param data
   */
  const mergeCurrentBookWithApiData = (bookId, uid, data = {}) => {
    let userBook = books.filter(b => b.id === bookId)[0];
    if(!userBook) userBook = {};
    return {
      ...userBook,
      ...data,
      ...{uid}
    };
  }

  let value = { books, setBooks, addBook, deleteBook, updateBook, onBookAction, mergeCurrentBookWithApiData };

  return (
    <BooksContext.Provider value={ value }>
      { children }
    </BooksContext.Provider>
  )
}

export default BooksProvider;

export const useBooks = () => useContext(BooksContext);